#include "SllClass.h"
#include "SLLmethods.h"

int main()
{

   Sequence<int,int> sequence_one;


   cout<<".......TESTING EMPTY LIST: START................"<<endl;
   sequence_one.print();
   cout<<"IS THE LIST EMPTY? "<<sequence_one.isEmpty()<<endl;
   cout<<"HOW MANY ELEMENTS ARE IN HE LIST? "<<sequence_one.length()<<endl;
   sequence_one.printByKey(12);
   sequence_one.deleteNodeByKey(28);
   sequence_one.delete_all_with_key(5);
   sequence_one.delete_first();
   sequence_one.delete_last();
   sequence_one.countKey(99);
   sequence_one.searchByKey(87);
   sequence_one.initializeList();
   sequence_one.destroyList();//no output message
   sequence_one.PrintInfoByKey(314);
   cout<<".......TESTING EMPTY LIST: END..................."<<endl;

   /*.......this 2 methods require separate testing because of the way assert work....*/
   //equence_one.FirstElement();
   //sequence_one.LastElement();


   cout<<"..........TESTING ISERTION IN THE LIST:START................"<<endl;
   Sequence<int,int> sequence_two;
   sequence_two.PushFirst(20,100);
   sequence_two.PushFirst(10,90);

   sequence_two.PushLast(30,110);
   sequence_two.PushLast(40,120);
   sequence_two.PushLast(100,111);
   sequence_two.PushLast(200,222);
   sequence_two.PushLast(300,333);

   sequence_two.SortInsert(133,1313);
   sequence_two.SortInsert(277,2727);

   sequence_two.PushAfter(40,45,4545);
   sequence_two.PushAfter(10,15,1515);
   sequence_two.PushAfter(300,310,310310);

   sequence_two.insertbefore(10,9,99);
   sequence_two.insertbefore(40,39,3939);
   sequence_two.insertbefore(310,305,305305);
   sequence_two.print();


   cout<<"..........TESTING ISERTION IN THE LIST:END................"<<endl;

    cout<<"...........Returning INFO FUNCTIONS TESTING........."<<endl;
   cout<<"FIRST ELEMENTS INFO:"<<sequence_two.FirstElement()<<endl;
   cout<<"LAST ELEMENT INFO:"<<sequence_two.LastElement()<<endl;
   cout<<"INFO WITH KEY 40: "<<endl;
   sequence_two.PrintInfoByKey(40);
   cout<<"INFO WITH KEY 9756 (NOT EXIST): "<<sequence_two.PrintInfoByKey(9756)<<endl;

   cout<<".............TESTING OPERATOR = (START).............."<<endl;
   Sequence<int,int> sequence_three;
   sequence_three = sequence_two;
   sequence_three.print();
   cout<<".............TESTING OPERATOR = (END).............."<<endl;

   cout<<"............TESTING DELETION FROM THE LIST:START..........."<<endl;


   cout<<"...........TESTING DELETING FROM THE LIST WITH ONE ELEMENT:START............"<<endl;
    Sequence<int,int> one_element_sequence;
   one_element_sequence.PushLast(500,5454);
   one_element_sequence.print();
   cout<<"................DELETE FIRST............"<<endl;
   one_element_sequence.delete_first();
   one_element_sequence.print();
   cout<<".....adding 600........."<<endl;
   one_element_sequence.PushLast(600,6464);
   one_element_sequence.print();
   cout<<"................DELETE LAST............"<<endl;
   one_element_sequence.delete_last();
   one_element_sequence.print();
   cout<<"................ADDING 700.............."<<endl;
   one_element_sequence.PushLast(700,7474);
   one_element_sequence.print();
   cout<<"................DELETE NODE BY KEY 700............"<<endl;
   one_element_sequence.deleteNodeByKey(700);
   one_element_sequence.print();
    cout<<"................ADDING 800.............."<<endl;
   one_element_sequence.PushLast(800,8484);
   one_element_sequence.print();
   cout<<"................DELETE ALL WITH KEY 800..........."<<endl;
   one_element_sequence.deleteNodeByKey(800);
   one_element_sequence.print();
   cout<<"...........TESTING DELETING FROM THE LIST WITH ONE ELEMENT:END............"<<endl;
   cout<<endl;

   cout<<"...........TESTING DELETING FROM THE LIST WITH MANY ELEMENTS:START............"<<endl;
   sequence_two.print();
   cout<<"...........DELETENODEBYKEYFUNCTION - KEYS:9,45,310 - ARE TO BE REMOVED............"<<endl;
   sequence_two.deleteNodeByKey(45); //deleting in the middle of a list (sequence_two)
   sequence_two.deleteNodeByKey(310);//deleting last element of a list (sequence_two)
   sequence_two.deleteNodeByKey(9); // deleting first element of a list (sequence_two)
   sequence_two.print();
   cout<<"................DELETE FIRST AND DELETE LAST............"<<endl;
   sequence_two.delete_first();
   sequence_two.delete_last();
   sequence_two.print();

    cout<<"............TESTING DELETING ALL ELEMENTS WITH THE SAME KEY:START..........."<<endl;
    cout<<"............ADDING ELEMENTS WITH THE SAME KEY IN THE BEGINNIG,MIDDLE AND END OF THE LIST..........."<<endl;
    sequence_two.PushFirst(1100,1212);//some elements to the beginning
    sequence_two.PushFirst(1100,1313);
    sequence_two.PushFirst(1100,1414);
    sequence_two.PushAfter(100,2100,2121);//some elements to the middle
    sequence_two.PushAfter(2100,2100,2222);
    sequence_two.PushAfter(2100,2100,2323);
    sequence_two.PushLast(3100,3131);//some elements to the end
    sequence_two.PushLast(3100,3232);
    sequence_two.PushLast(3100,3333);
    sequence_two.print();
    cout<<"...............DELETING ALL KEYS 1100............"<<endl;
    sequence_two.delete_all_with_key(1100);
    sequence_two.print();

    cout<<"...............DELETING ALL KEYS 2100............"<<endl;
    sequence_two.delete_all_with_key(2100);
    sequence_two.print();

    cout<<"...............DELETING ALL KEYS 3100............"<<endl;
    sequence_two.delete_all_with_key(3100);
    sequence_two.print();

     cout<<"...............DELETING ALL KEYS 5500(in random places)............"<<endl;
     cout<<"...............ADDING 5500............"<<endl;
     sequence_two.PushAfter(39,5500,5151);//Adding elements to random places
     sequence_two.PushAfter(40,5500,5252);
     sequence_two.PushAfter(100,5500,5353);
     sequence_two.PushAfter(133,5500,5454);
     sequence_two.print();

     cout<<"...............DELETING 5500............"<<endl;
     sequence_two.delete_all_with_key(5500);
     sequence_two.print();

  cout<<"............TESTING DELETION FROM THE LIST:END..........."<<endl;

   cout<<"............TESTING METHODS WITH NON EXISTING ELEMENTS:START..........."<<endl;

   sequence_two.deleteNodeByKey(777);// non existing element
   sequence_two.delete_all_with_key(847);
   cout<<"IS THERE AN ELEMENT WITH KEY 999 IN THE LIST? "<<sequence_two.searchByKey(999)<<endl;
   sequence_two.countKey(656);
   sequence_two.PrintInfoByKey(7333);
   sequence_two.printByKey(7778);
   cout<<"............TESTING METHOD WITH NON EXISTING ELEMENTS:END..........."<<endl;
   cout<<"LENGHT OF SEQUENCE 2 NOW IS:"<<sequence_two.length()<<" ELEMENTS"<<endl;

   cout<<"...........DESTROYING SEQUENCE 2........."<<endl;
   sequence_two.destroyList();
   sequence_two.print();



    return 0;


}
