#ifndef SLLMETHODS_H_INCLUDED
#define SLLMETHODS_H_INCLUDED

template <typename Key,typename Info>
Sequence<Key,Info>::Sequence() //default constructor
{
    first = nullptr;
    last = nullptr;
    count = 0;
    cout<<"...Default Constructor did his job...."<<endl;
}

template <typename Key,typename Info>
Sequence<Key,Info>::Sequence(const Sequence<Key,Info>& otherList)
{
   	first = nullptr;
   	//we initialize first to nullptr because the checking of it is done in Copyist function
    copyList(otherList);
    cout<<"Copy constructor did his job"<<endl;
}//end copy constructor


template <typename Key,typename Info>
const Sequence<Key,Info>& Sequence<Key,Info>::operator=
                      (const Sequence<Key,Info>& otherList)
{
    if (this != &otherList) //avoid self-copy
    {
        copyList(otherList);
    }//end else

     return *this;
}

template <typename Key,typename Info>
void Sequence<Key,Info>::initializeList()
{
	if(first == nullptr)
    {
        cout<<"InitializeList:List is already empty"<<endl;
    }
	destroyList(); //if the list has any nodes, delete them
}

template <typename Key,typename Info>
void Sequence<Key,Info>::destroyList()
{

    Node* temp;   //pointer to deallocate the memory
                            //occupied by the node
    while (first != nullptr)   //while there are nodes in the list
    {
        temp = first;        //set temp to the current node
        first = first->next; //advance first to the next node
        delete temp;   //deallocate the memory occupied by temp
    }

    last = nullptr; //initialize last to NULL;
    //no need of assigning first to null because first has already been set to NULL by the while loop
    count = 0;
}

template <typename Key,typename Info>
Sequence<Key,Info>::~Sequence() //destructor
{
   destroyList();
     cout<<"...Destructor did his job...."<<endl;
}//end destructor

template <typename Key,typename Info>
void Sequence<Key,Info>::copyList
                   (const Sequence<Key,Info>& otherList)
{
    Node *newNode; //pointer to create a node
    Node *current; //pointer to traverse the list

    if (first != nullptr) //if the list is nonempty, make it empty
       destroyList();

    if (otherList.first == nullptr) //otherList is empty
    {
        first = NULL;
        last = NULL;
        count = 0;
    }
    else
    {
        current = otherList.first; //current points to the
                                   //list to be copied
        count = otherList.count;

            //copy the first node
        first = new Node;  //create the node

        first->key = current->key; //copy the info
        first->info = current->info;
        first->next = nullptr;        //set the link field of
                                   //the node to NULL
        last = first;              //make last point to the
                                   //first node
        current = current->next;     //make current point to
                                     //the next node

           //copy the remaining list
        while (current != nullptr)
        {
            newNode = new Node;  //create a node
            newNode->key = current->key; //copy the info
            newNode->info = current->info;
            newNode->next = nullptr;       //set the link of
                                        //newNode to NULL
            last->next = newNode;  //attach newNode after last
            last = newNode;        //make last point to
                                   //the actual last node
            current = current->next;   //make current point
                                       //to the next node
        }//end while
    }//end else
}//end copyList



template <typename Key,typename Info>
bool Sequence<Key,Info>::isEmpty() const
{
    return (first == nullptr);
}

template <typename Key,typename Info>
int Sequence<Key,Info>:: length() const
{
    return count;
}  //end length


template <typename Key,typename Info>
Info Sequence<Key,Info>::FirstElement() const
{
    assert(first != nullptr); //if 0 than the message recorded on standard output device and program is terminated

    return first->info; //return the info of the first node
}


template <typename Key,typename Info>
Info Sequence<Key,Info>::LastElement() const
{
    assert(last != nullptr);

    return last->info; //return the info of the last node
}

template <typename Key,typename Info>
bool Sequence<Key,Info> :: PrintInfoByKey(const Key& someKey)
{
    if(first == nullptr)
    {
        cout<<"PrintInfoByKey:The list is empty"<<endl;
        return 0;

    }
   else if(searchByKey(someKey) == false)
   {
       cout<<"PrintInfoByKey: No element with such a Key in the list"<<endl;
       return 0;

   }
   else
   {
    cout<<"PrintInfoByKey: In the list there are: "<< this->countKey(someKey) <<" element(s) of Given Key"<<endl;
    cout<<"Info of the first element with this key will be displayed"<<endl;


    Node* current;

    current = first;
            while(current!=nullptr)
    {
        if(current->key==someKey) break;
        current=current->next;

    }

            cout<<"INFO OF THE KEY: "<<someKey<<" IS: "<< current->info<<endl;
            return 1;
    }

}


template <typename Key,typename Info>
bool Sequence<Key,Info>::searchByKey(const Key& searchItemKey) const
{
    if(first == nullptr)
    {
        cout<<"searchbyKey:The list is empty"<<endl;
        return 0;
    }
   else
   {

    bool found = false;
    Node *current; //pointer to go through the list


    current = first; //set current to point to the first
                     //node in the list to start from the first node

    while (current != nullptr && found == false)    //search the list //AND GIVES TRU PNLY IF BOTh ELEMENTS ARE TRUE
        if (current->key == searchItemKey) //searchItem is found
            found = true;
        else
            current = current->next; //make current point to

                                     //the next node
    return found;
   }
}//end search


template <typename Key,typename Info>
int Sequence<Key,Info>:: countKey(const Key& givenKey) // counts all elements with given Key
	{
		if (first==nullptr)
        {
            cout<<"countKey:The list is empty"<<endl;
            return 0;
        }
        else if(searchByKey(givenKey)==false)
        {
            cout<<"CountKey:No such a Key in the list"<<endl;
            return 0;
        }
    else
        {
        int counter = 0;
		Node* Current = first;

		while (Current!=nullptr)
		{
			if (Current->key == givenKey)
                counter++;
			Current = Current->next;
		}

		return counter;
        }
	}


/* ............Inserting related methods starts here ........ */

template <typename Key,typename Info>
void Sequence<Key,Info>::PushFirst(const Key& FsomeKey,const Info& FsomeInfo){
Node * newNode; //pointer to create a new node
newNode = new Node;
newNode->key = FsomeKey;
newNode->info = FsomeInfo;
newNode->next = first;
first = newNode;
count++;

if(last == nullptr)
{
    last = newNode;
}

/*insertMid(FsomeKey,FsomeInfo);*/
}//end PushFirst


template <typename Key,typename Info>
bool Sequence<Key,Info>::PushAfter(const Key& KeyAfter,const Key& AsomeKey, const Info& AsomeInfo)
{


  if(first == nullptr)
   {
       cout<<"The List is Empty,Better Use PushFirst or SortInsert"<<endl;
       return 0;
   }
   else
{
    Node * newNode;
   newNode = new Node;
   newNode->key = AsomeKey;
   newNode->info = AsomeInfo;
   newNode->next = nullptr;

   Node * current;

   current = first;

    while(current->key!=KeyAfter)
   {
      current=current->next;

      if(current == nullptr)
      {
          cout<<".....NO SUCH A KEY IN THE LIST (function PushAfter)...."<<endl;
          return 0;
      }
   }


  if(current->key == KeyAfter && current->next==nullptr) //if we want to push element after the last element
  {
      current->next = newNode;
      newNode->next = nullptr;
      last = newNode;
      count++;
  }
  else //anywhere else in the list
  {
  newNode->next = current->next;
  current->next = newNode;
  count++;
   }

}
}

template <typename Key,typename Info>
void Sequence<Key,Info>::PushLast(const Key& LsomeKey,const Info& LsomeInfo)
{
    Node * newNode; //pointer to create the new node

    newNode = new Node; //create the new node

    newNode->key = LsomeKey;//store the new item in the node
    newNode->info = LsomeInfo;
    newNode->next = nullptr;     //set the link field of newNode
                              //to NULL

    if (first == nullptr)  //if the list is empty, newNode is
                        //both the first and last node
    {
        first = newNode;
        last = newNode;
        count++;        //increment count
    }
    else    //the list is not empty, insert newNode after last
    {
        last->next = newNode; //insert newNode after last
        last = newNode; //make last point to the actual
                        //last node in the list
        count++;        //increment count
    }

}//end PushLast

template <typename Key,typename Info>
void Sequence<Key,Info>:: SortInsert(const Key& MsomeKey,const Info& MsomeInfo){
    Node* current; //pointer to traverse the list
    Node* beforeCurrent; //pointer just before current
    Node* newNode;  //pointer to create a node

    bool  found;

    newNode = new Node; //create the node
    newNode->key = MsomeKey;   //store newItem in the node
    newNode->info = MsomeInfo;
    newNode->next = nullptr;      //set the link field of the node
                               //to NULL

    if (first == nullptr)  //Case 1 list is empty
    {
        first = newNode;
        last = newNode;
        count++;
    }
    else
    {
        current = first; //list is not empty
        found = false;

        while (current != nullptr && found == false) //search the list
           if (current->key >= MsomeKey)
               found = true;
           else
           {
               beforeCurrent = current;   //keep searching
               current = current->next;
           }

        if (current == first)      //Case 2 (new key is smaller than the smallest key in the list - >so it goes to the beginning)
        {
            newNode->next = first;
            first = newNode;
            count++;
        }
        else                       //Case 3 to be inserted somewhere in the list
        {
            beforeCurrent->next = newNode;
            newNode->next = current;

            if (current == nullptr)  //the situation if u want to add the element with the grates key to the list
                last = newNode;

            count++;
        }
    }//end else
}//end insert


template <typename Key,typename Info>
void Sequence<Key,Info>::insertbefore(const Key& chosen,const Key& what,const Key& newinfo)
{

    Node* current;
    Node* beforecurrent;
    bool exist = false;

    if(first == nullptr)
        {
            cout<<"insertbefore:The list is empty"<<endl;
        //delete newNode;
           return;
        }
   else
   {
       current = first;
       while(current!=nullptr && exist==false )
       {
           if(current->key == chosen)
             {

                 Node*newNode = new Node;
                 newNode->key = what;
                 newNode->info = newinfo;
                 newNode->next = current;
                 if(newNode->next == first)
                   {first = newNode;
                   }

                else{
            beforecurrent->next = newNode;
                 }
            count++;
            exist = true;
            }

           beforecurrent = current;
           current = current->next;

      }
      if(exist == false)
        cout<<"No such a key chosen in the list"<<endl;

}
}

/* ............Inserting related methods ends here ........ */


/* ............Deleting related methods start here ........ */

template <typename Key,typename Info>
void Sequence<Key,Info>::deleteNodeByKey(const Key& someKey)
{
    Node*current; //pointer to traverse the list
    Node* beforeCurrent; //pointer just before current
    bool found;

    if (first == nullptr)    //Case 1; the list is empty.
        cout << "deleteNodeByKey:Cannot delete from an empty list."<< endl;
    else
    {
        if (first->key == someKey) //Case 2
        {
            current = first;
            first = first->next;
            count--;
            if (first == nullptr)    //the list has only one node
                last = nullptr;
            delete current;
        }
        else //search the list for the node with the given info
        {
            found = false; //to be used in a loop
            beforeCurrent = first;  //set beforeCurrent to point
                                   //to the first node
            current = first->next; //set current to point to
                                   //the second node (because now we are going to start searching from second node

            while (current != nullptr && !found)
            {
                if (current->key != someKey)
                {
                    beforeCurrent = current;
                    current = current-> next;
                }
                else
                    found = true;
            }//end while

            if (found) //Case 3; if found, delete the node
            {
                beforeCurrent->next = current->next;
                count--;

                if (last == current)   //node to be deleted
                                       //was the last node
                    last = beforeCurrent; //update the value
                                         //of last
                delete current;  //delete the node from the list
            }
            else
                cout << "deleteNodeByKey:The item to be deleted is not in "
                     << "the list." << endl;
        }//end else
    }//end else
}//end deleteNode


template <typename Key,typename Info>
void Sequence<Key,Info>:: delete_first() // removes first element in the list
	{
		if (first == nullptr)
             {
                 cout<<"delete_first:Empty List"<<endl;
                 return;
            }

		Node* Current = first;
		first = Current->next;
		if(first == nullptr)
            last = nullptr;
            count--;
		delete Current;
	}


template <typename Key,typename Info>
void Sequence<Key,Info>::delete_last() // removes last element in the list
	{
		if (first == nullptr)
            {
               cout<<"delete_last:Empty List"<<endl;
               return;
            }


       else if(first->next == nullptr)
        {
            Node* temp = first;
            first = nullptr;
            last = nullptr;
            count--;
            delete temp;
        }
   else
      {
      Node* beforeCurrent;
      Node* Current = first;
      while (Current->next!=nullptr)
		{
			beforeCurrent = Current;
			Current = Current->next;
		}
		beforeCurrent->next = nullptr;
		last = beforeCurrent;
		count--;
		delete Current;
	}
	}



    //Possible variation of delete all with key method

template <typename Key,typename Info>
void Sequence<Key,Info>::delete_all_with_key(Key givenKey) // removes all elements with Key == givenKey
	{
		if(first == nullptr)
        {
             cout<<"delete_all_with_key:EMPTY list"<<endl;
            return;
        }
		else if(searchByKey(givenKey)==false)
        {
            cout<<"delete_all_with_key:No such and element in the list"<<endl;
            return;
        }
        else
        {

		bool Exist = searchByKey(givenKey);

		while (Exist)
		{
			deleteNodeByKey(givenKey);
			Exist = searchByKey(givenKey);
		}
        }
	}


/* ............Deleting related methods end here ........ */



template <typename Key,typename Info>
void Sequence<Key,Info>::print() const
{


    Node *current; //pointer to traverse the list

    current = first;    //set current so that it points to
                        //the first node
    int i = 1;
    if(current == nullptr)
    {
        cout<<"print function:The list is empty"<<endl;
        cout<<endl;
    }
    else
    {
    while (current != nullptr) //while more data to print
    {
        cout<<"Element N: "<<i<<endl;
        cout <<"KEY: "<<current->key<<endl<<"INFO: "<<current->info<<endl;
        cout<<endl;
        i++;
        current = current->next;
    }
      }
}//end print


template <typename Key,typename Info>
void Sequence<Key,Info>::printByKey(const Key& PsomeKey) const{

if(first == nullptr)
  {
      cout<<"printByKey:EMPTY list"<<endl;
      return;
  }

Node* current;
current = first;


  while(current != nullptr ){
    if(current->key == PsomeKey)
    {
        cout<<"SEARCHED ELEMENT: "<<endl;
        cout<<"KEY: "<<current->key<<endl<<"INFO: "<<current->info<<endl;
        break; }

        else
            current=current->next;
       if(current == nullptr)
          cout<<"printByKey : NOT FOUND"<<endl;



  }

  }



#endif // SLLMETHODS_H_INCLUDED
