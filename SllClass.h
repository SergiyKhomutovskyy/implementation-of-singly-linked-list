#ifndef SLLCLASS_H_INCLUDED
#define SLLCLASS_H_INCLUDED

#include <iostream>
#include <cassert>

using namespace std;

template <typename Key,typename Info>
 class Sequence{

private:

    struct Node{
Key key;
Info info;
Node *next;
};
    Node * last; //pointer on the last element of the list
    int count; //will be storing the number of elements in the list
    Node * first;//pointer on the first element of the list

public:
     Sequence();//default constructor

     Sequence(const Sequence<Key,Info>& otherList);
      //copy constructor

    ~Sequence();//destructor

       void initializeList();
      //Initialize the list to an empty state.
      //such that: first = NULL, last = NULL, count = 0;

   void copyList(const Sequence<Key,Info>& otherList);
      //Function to make a copy of otherList.
      //to be used in copy constructor later on
      //Such that: A copy of otherList is created and assigned
      //    to this list.

  const Sequence<Key,Info>& operator=(const Sequence<Key,Info>&);
      //Overload the assignment operator.

    bool isEmpty() const;
     //Function to determine whether the list is empty.
      // Returns true if the list is empty,
      // otherwise it returns false.

    int length() const;
     //Function to return the number of nodes in the list.
      // The value of count is returned.

    Info FirstElement() const;
      //Function to return info of the first element of the list.
      //The list must exist and must not be empty.
      // If the list is empty, the program terminates;
      //    otherwise, the first element of the list is returned.

    Info LastElement() const;
      //Function to return info of the last element of the list.
      // The list must exist and must not be empty.
      //If the list is empty, the program
      //               terminates; otherwise, the last
      //               element of the list is returned.

    bool PrintInfoByKey(const Key& someKey);
    // Function which displays info of the key which was passed to this function
    // if in the list there are more than 1 elements with the key that was passed
    // prints the info of the element that appeared first
    // if there is only one element with the given key,function displays its info
    // and returns 1;
    //if there is no such a key returns 0

    bool searchByKey(const Key& searchItemKey) const;
        //Function to determine whether searchItem (Node) is in the list.
      //Returns true if searchItem is in the list,
      //    otherwise the value false is returned.

	int countKey(const Key& givenKey);
    // counts all elements with given Key

    void PushFirst(const Key& FsomeKey,const Info& FsomeInfo);
      //Function to insert newItem at the beginning of the list.
      //  newItem is inserted at the beginning of the list, last points to the
      //    last node in the list, and count is incremented by 1.

    bool PushAfter(const Key& KeyAfter, const Key& AsomeKey, const Info& AsomeInfo);
      //Function to insert newNode at the specific position(after the required key)

    void PushLast(const Key& LsomeKey,const Info& LsomeInfo);
    //Function to insert newItem at the end of the list.
      //  newItem is inserted at the end of the list, last points to the
      //    last node in the list, and count is incremented by 1.

      void SortInsert(const Key& MsomeKey,const Info& MsomeInfo);
      //Function to insert newItem in the list.
      //  newItem is inserted at the proper place in the list, and
      //    count is incremented by 1.

      void insertbefore(const Key& chosen,const Key& what,const Key& newinfo);
      //inserts a node before a specific node in the list

    void deleteNodeByKey(const Key& DsomeKey);
        //Function to delete deleteItem from the list.
      //Postcondition: If found, the node containing deleteItem is
      //    deleted from the list; first points to the first node
      //    of the new list, and count is decremented by 1. If
      //    deleteItem is not in the list, an appropriate message
      //    is printed.

      void delete_first();
      // delete first element in the list

      void delete_last();
      	// removes last element in the list

      void destroyList();
      //Function to delete all the nodes from the list.
      //so: first = NULL, last = NULL, count = 0;
      //to be used in destructor

      void print() const;
      //function to output all nodes which the list contains

      void printByKey(const Key& PsomeKey) const;
      //function to output one particular node

      void delete_all_with_key(Key givenKey);
      	// removes all elements with Key == givenKey


 };


#endif // SLLCLASS_H_INCLUDED
